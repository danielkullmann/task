# Example project for Django, React and Keycloak

* Backend: Django
* Authentication provider: Keycloak
* Frontend: React
* Database: PostgreSQL (for both Django and Keycloak)
* Local development: docker-compose with 4 docker containers,
  one for each part mentioned above
* Deployment: TBD (Kubernetes?)

This blog post helped a lot setting this up:
https://blog.logrocket.com/using-react-django-create-app-tutorial/; 
source code for this blog post is in
https://github.com/diogosouza/django-react-logrocket

## Authentication

To make the authentication work, I used
https://github.com/AlTosterino/django-react-keycloak
as an example. The process is as follows:

* The React app uses keycloak-js to authenticate the user against Keycloak
* When making calls to the backend, the React app uses the token from keycloak
  as a Bearer Authentication token
* The Django application uses the keycloak module to check whether the Bearer
  token can be resolved to a user, and then acts accordingly

