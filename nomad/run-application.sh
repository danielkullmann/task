#!/bin/sh

export NOMAD_ADDR=http://192.168.141.1:4646

BASE_PATH=$(dirname $(readlink -f $(dirname "$0")))

docker build "$BASE_PATH/api" --tag task-api:local
docker build "$BASE_PATH/frontend" --tag task-frontend:local

# Put all variables
nomad var put -force nomad/jobs \
    DB_NAME=task \
    DB_USER=task-user \
    DB_PASSWORD=r9glknas2ndf2v \
    KEYCLOAK_ADMIN_USER=admin \
    KEYCLOAK_ADMIN_PASSWORD=admin \
    KEYCLOAK_SERVER=http://task-keycloak:8080/ \
    KEYCLOAK_REALM=task \
    KEYCLOAK_CLIENT_ID=webapp

nomad run postgresql.nomad.hcl
nomad run keycloak.nomad.hcl
nomad run task-api.nomad.hcl
nomad run task-frontend.nomad.hcl


