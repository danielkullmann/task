job "postgresql" {
  type = "service"

  constraint {
    attribute = "${attr.kernel.name}"
    value     = "linux"
  }

  update {
    max_parallel = 1
    min_healthy_time = "10s"
    healthy_deadline = "10m"
    progress_deadline = "11m"
    auto_revert = false
    canary = 0
  }

  group "database" {

    network {
      port "database" {
        static = 5432
      }
    }

   volume "postgresql" {
      type      = "host"
      read_only = false
      source    = "postgresql"
    }

    service {
      name     = "postgresql"
      tags     = ["global", "database"]
      port     = "database"
      provider = "nomad"

      # check {
      #   name     = "alive"
      #   type     = "tcp"
      #   interval = "10s"
      #   timeout  = "2s"
      # }

    }

    task "postgresql" {
      driver = "docker"

      config {
        image = "postgres"
        ports = ["database"]
        args = ["-p", "5432"]
      }

      volume_mount {
        volume      = "postgresql"
        destination = "/var/lib/postgresql/data"
        read_only   = false
      }

      identity {
        env  = true
        file = true
      }

      resources {
        cpu    = 500
        memory = 1024
      }

      template {
        data = <<EOF
{{ with nomadVar "nomad/jobs/" }}
        POSTGRES_USER = "{{ .DB_USER }}"
        POSTGRES_PASSWORD = "{{ .DB_PASSWORD }}"
        POSTGRES_DB = "{{ .DB_NAME }}"
{{ end }}
EOF
        destination = "local/env.txt"
        env         = true
      }

    }
  }
}
