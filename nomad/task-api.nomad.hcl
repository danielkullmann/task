job "task-api" {

  type = "service"

  constraint {
    attribute = "${attr.kernel.name}"
    value     = "linux"
  }

  update {
    max_parallel = 1
    min_healthy_time = "10s"
    healthy_deadline = "10m"
    progress_deadline = "11m"
    auto_revert = false
    canary = 0
  }

  group "api" {
    count = 1

    network {
      port "api" {
        static = 8000
      }
    }

    service {
      name     = "task-api"
      tags     = ["global", "api"]
      port     = "api"
      provider = "nomad"
    }

    task "task-api" {
      driver = "docker"

      config {
        image = "task-api:local"
        ports = ["api"]
      }

      identity {
        env  = true
        file = true
      }

      resources {
        cpu    = 500
        memory = 256
      }

      template {
        data = <<EOF
{{ with nomadVar "nomad/jobs/" }}
        DB_USER = "{{ .DB_USER }}"
        DB_PASSWORD = "{{ .DB_PASSWORD }}"
        DB_NAME = "{{ .DB_NAME }}"
        KEYCLOAK_REALM = "{{ .KEYCLOAK_REALM }}"
        KEYCLOAK_CLIENT_ID = "{{ .KEYCLOAK_CLIENT_ID }}"
{{ end }}

{{ range nomadService "postgresql" }}
        DB_HOST="{{ .Address }}"
        DB_PORT="{{ .Port }}"
{{ end }}

{{ range nomadService "keycloak" }}
        KEYCLOAK_SERVER = "http://{{ .Address }}:{{ .Port }}/"
{{ end }}
EOF
        destination = "local/env.txt"
        env         = true
      }
    }
  }
}
