# Deployment via Nomad

This uses Hashicorp's Nomad to deploy the application.

## Local development

Somehow, Nomad doesn't like the local addresses 127.x.y.z, so I tried to setup a new loopback device with a new address:

```
sudo ip link add name loop1 type dummy
sudo ip addr add 192.168.141.1/32 dev loop1
(echo ""; echo "192.168.141.1   server.local"; echo "") | sudo tee -a /etc/hosts

```

Now, you can start the local Nomad cluster with:

```
./start-dev-nomad-cluster.sh
```

And then run the parts of our application (postgresql, keycloak, api, frontend) on the cluster:

```
./run-application.sh
```

To stop the application, you can run:

```
./stop-application.sh
```

To use the `nomad` cli, you have to set the address of the Nomad server:

```
export NOMAD_ADDR=http://192.168.141.1:4646
```

### Notes

**IMPORTANT** The first time the app is started, we need to create the database for keycloak. So first issue a `docker ps` to
find the name of the postgres container, then create the database:

```
docker exec -it postgresql-eb66a53f-baf7-6175-d082-b0233c55756d
createdb -h localhost -U task-user keycloak
```

(The task to automate this when the postgresql container is started is a TODO).


Nomad doesn't work well with  `:latest` tags on Docker images, you need to build the docker images with another tag
(this is done by the script `./run-application.sh`)

```
docker build api --tag task-api:local
docker build frontend --tag task-frontend:local
```
