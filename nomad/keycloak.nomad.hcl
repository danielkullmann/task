job "keycloak" {

  type = "service"

  constraint {
    attribute = "${attr.kernel.name}"
    value     = "linux"
  }

  update {
    max_parallel = 1
    min_healthy_time = "10s"
    healthy_deadline = "10m"
    progress_deadline = "11m"
    auto_revert = false
    canary = 0
  }

  group "keycloak" {
    count = 1

    network {
      port "keycloak" {
        static = 8080
      }
    }

    service {
      name     = "keycloak"
      tags     = ["global", "keycloak"]
      port     = "keycloak"
      provider = "nomad"
    }

    task "keycloak" {
      driver = "docker"

      config {
        image = "quay.io/keycloak/keycloak:22.0.3"
        ports = ["keycloak"]
        args = ["start-dev", "--hostname", "server.local"]
      }

      identity {
        env  = true
        file = true
      }

      resources {
        cpu    = 500
        memory = 512
      }

      template {
        data = <<EOF
{{ with nomadVar "nomad/jobs/" }}
        KEYCLOAK_ADMIN_USER = "{{ .KEYCLOAK_ADMIN_USER }}"
        KEYCLOAK_ADMIN_PASSWORD = "{{ .KEYCLOAK_ADMIN_PASSWORD }}"
        KC_DB_USERNAME = "{{ .DB_USER }}"
        KC_DB_PASSWORD = "{{ .DB_PASSWORD }}"
{{ end }}
        KC_DB = "keycloak"
        KC_HOSTNAME = "localhost"

{{ range nomadService "postgresql" }}
        KC_DB_URL="jdbc:postgresql://{{ .Address }}:{{ .Port }}/keycloak"
{{ end }}
EOF
        destination = "local/env.txt"
        env         = true
      }
    }
  }
}
