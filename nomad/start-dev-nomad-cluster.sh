#!/bin/sh

BASE_PATH=$(readlink -f $(dirname "$0"))

sed -e "s%__BASE_PATH__%$BASE_PATH%" "$BASE_PATH/nomad-client.conf.hcl.template" > "$BASE_PATH/nomad-client.conf.hcl"

sudo nomad agent -dev-connect -network-interface loop1 -config "$BASE_PATH/nomad-client.conf.hcl"
