job "task-frontend" {
 
  type = "service"

  constraint {
    attribute = "${attr.kernel.name}"
    value     = "linux"
  }

  update {
    max_parallel = 1
    min_healthy_time = "10s"
    healthy_deadline = "10m"
    progress_deadline = "11m"
    auto_revert = false
    canary = 0
  }

  group "task-frontend" {
    count = 1

    network {
      port "frontend" {
        static = 80
      }
    }

    service {
      name     = "task-frontend"
      tags     = ["global", "task-frontend"]
      port     = "frontend"
      provider = "nomad"
    }

    task "task-frontend" {
      driver = "docker"

      config {
        image = "task-frontend:local"
        ports = ["frontend"]
      }

      identity {
        env  = true
        file = true
      }

      resources {
        cpu    = 500
        memory = 256
      }

    }
  }
}
