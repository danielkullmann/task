# Deployment of application via Helm

This allows deployment of the application into a Kubernetes cluster via Helm.

## Before deployment

You need access to a Kubernetes cluster. For local testing, minikube can be used.
So first, install minikube; then start the cluster:

```sh
minikube start
```

Docker images can be built into the docker daemon of the Kubernetes cluster:

```
eval $(minikube -p minikube docker-env)
docker build api --tag task-api
docker build frontend --tag task-frontend
```


## How to deploy

```sh
  cd helm/task
  helm dependency update
```