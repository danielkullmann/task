import React, { Component } from "react";
import { Col, Container, Row } from "reactstrap";
import axios from "axios";
import { withKeycloak } from "@react-keycloak/web";

import TaskList from "./TaskList";
import { API_URL } from "../constants";

class Main extends Component {
  state = {
    tasks: []
  };

  componentDidMount() {
    this.resetState();
  }

  getTasks = () => {
    const auth = this.props.keycloak;
    if (auth.authenticated) {
      console.log("AUTH", auth);
      axios.get(
        API_URL + "tasks/", {
          headers: {
            Authorization: "Bearer " + auth.token
          }
        }).then(res => this.setState({ tasks: res.data }));
    } else {
      console.log("No AUTH; do not load tasks");
    }
  };

  resetState = () => {
    console.log("reset state");
    this.getTasks();
  };


  render() {
    const auth = this.props.keycloak;
    console.log("AUTH in render", auth.authenticated);
    if (!auth.authenticated) {
      return <button onClick={() => void auth.login()}>Log in</button>;
    }

    return (
      <Container style={{ marginTop: "20px" }}>
        <Row>
          <Col>
            <TaskList
              tasks={this.state.tasks}
              resetState={this.resetState}
            />
          </Col>
        </Row>
      </Container>
    );
  }
}

export default withKeycloak(Main);

