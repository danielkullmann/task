import React, { Component } from "react";
import { Table } from "reactstrap";

class TaskList extends Component {
  render() {
    const tasks = this.props.tasks;
    const resetState = this.props.resetState;
    return (
      <Table dark>
        <thead>
          <tr>
            <th>Name</th>
            <th>Description</th>
            <th>State</th>
            <th><button onClick={resetState}>R</button></th>
          </tr>
        </thead>
        <tbody>
          {!tasks || tasks.length <= 0 ? (
            <tr>
              <td colSpan="6" align="center">
                <b>No tasks available</b>
              </td>
            </tr>
          ) : (
            tasks.map(task => (
              <tr key={task.pk}>
                <td>{task.title}</td>
                <td>{task.description}</td>
                <td>{task.status.name}</td>
                <td>&nbsp;</td>
              </tr>
            ))
          )}
        </tbody>
      </Table>
    );
  }
}

export default TaskList;