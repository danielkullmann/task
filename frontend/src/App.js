import React, { Component, Fragment } from "react";
import { ReactKeycloakProvider } from "@react-keycloak/web";

import keycloak from "./keycloak";
import Header from "./components/Header";
import Main from "./components/Main";

class App extends Component {
  render() {
    return (
      <ReactKeycloakProvider authClient={keycloak}>
        <Fragment>
          <Header />
          <Main />
        </Fragment>
      </ReactKeycloakProvider>
    );
  }
}

export default App;