
export const API_URL = "/api/";

// TODO: switch value based on deployment type
// Local development:
//   export const KEYCLOAK_URL = "http://localhost:8080/";
// Local Nomad deployment:
//   export const KEYCLOAK_URL = "http://server.local:8080/";

export const KEYCLOAK_URL = "http://server.local:8080/";
