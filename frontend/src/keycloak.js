import Keycloak from "keycloak-js";
import { KEYCLOAK_URL } from "./constants";


const keycloak = new Keycloak({
 url: KEYCLOAK_URL,
 realm: "task",
 clientId: "webapp",
});
export default keycloak;