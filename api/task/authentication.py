import logging
import os

from django.contrib.auth.models import AnonymousUser
from keycloak import KeycloakOpenID, KeycloakError
from rest_framework.authentication import BaseAuthentication
from rest_framework.exceptions import AuthenticationFailed

KEYCLOAK_OPENID = KeycloakOpenID(
    server_url=os.getenv("KEYCLOAK_SERVER"),
    client_id=os.getenv("KEYCLOAK_CLIENT_ID"),
    realm_name=os.getenv("KEYCLOAK_REALM"),
)

logger = logging.getLogger(__name__)

class TokenUser(AnonymousUser):
    """
    Django Rest Framework needs an user to consider authenticated
    TODO: Make a model for that?
    """

    def __init__(self, user_info):
        super().__init__()
        self.user_info = user_info

    @property
    def is_authenticated(self):
        return True


class KeyCloakAuthentication(BaseAuthentication):
    def authenticate(self, request):
        token = request.headers.get("Authorization")
        if not token:
            raise AuthenticationFailed()

        try:
            _, token = token.split(" ")
            user_info = KEYCLOAK_OPENID.userinfo(token)
        except (AttributeError, KeycloakError) as e:
            logger.error("Keycloak error " + str(e))
            raise AuthenticationFailed()
        return (TokenUser(user_info=user_info), None)
