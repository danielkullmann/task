"""
URL configuration for task project.

https://docs.djangoproject.com/en/4.2/topics/http/urls/
"""
from django.contrib import admin
from django.urls import path

from api import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/tasks/', views.task_list),
    path('api/tasks/<int:pk>', views.task_detail),
]
