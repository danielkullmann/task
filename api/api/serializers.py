from rest_framework import serializers
from .models import Task, Status

class StatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = Status 
        fields = ('pk', 'name', 'order')

class TaskSerializer(serializers.ModelSerializer):
    status = StatusSerializer()

    class Meta:
        model = Task 
        fields = ('pk', 'title', 'description', 'status')
