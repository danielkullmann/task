from django.db import models

class Status(models.Model):
    name = models.CharField(max_length=64, blank=False)
    order = models.IntegerField(blank=False, default=0)
            
    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Status"

class Task(models.Model):
    title = models.CharField(max_length=128, blank=False)
    description = models.TextField(blank=True)
    status = models.ForeignKey(to=Status, on_delete=models.RESTRICT, blank=False)

    def __str__(self):
        return f"{self.title} (${self.status.name})"

