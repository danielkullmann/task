from django.contrib import admin

from api.models import Status, Task

@admin.register(Status)
class StatusAdmin(admin.ModelAdmin):
        list_display = ('pk', 'name', 'order')

@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
        list_display = ('pk', 'title', 'description', 'status')

