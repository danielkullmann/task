"""
Management utility to initialize Task app:
* create admin user if none exists
* create default Status entries if none exist
* create some dummy Task entries if None exist
"""

from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

from api.models import Status, Task


class Command(BaseCommand):
    help = "Used to create a superuser."
    requires_migrations_checks = True
    stealth_options = ("stdin",)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.UserModel = get_user_model()
        self.username_field = self.UserModel._meta.get_field(
            self.UserModel.USERNAME_FIELD
        )

    #def add_arguments(self, parser):
    #    pass

    def handle(self, **options):
        admin_users = self.UserModel.objects.filter(username="admin")
        if len(admin_users) == 0:
            # Create admin user
                admin = self.UserModel(username="admin", password="admin", is_superuser=True, is_staff=True)
                admin.save()

        status_list = Status.objects.all()
        todo_status, in_progress_status, review_status, done_status = None, None, None, None
        if len(status_list) == 0:
             # Create default status entried
             todo_status = Status(name="TODO", order=0)
             todo_status.save()
             in_progress_status = Status(name="IN_PROGRESS", order=1)
             in_progress_status.save()
             review_status = Status(name="REVIEW", order=2)
             review_status.save()
             done_status = Status(name="DONE", order=3)
             done_status.save()
             
        task_list = Task.objects.all()
        if len(task_list) == 0:
             if todo_status is None:
                  todo_status = Status.objects.filter(name="TODO").first()
             if todo_status is not None:
                todo_task = Task(title="task to do", status=todo_status)
                todo_task.save()

             if in_progress_status is None:
                  in_progress_status = Status.objects.filter(name="IN_PROGRESS").first()
             if in_progress_status is not None:
                in_progress_task = Task(title="task in progress", status=in_progress_status)
                in_progress_task.save()

             if review_status is None:
                  review_status = Status.objects.filter(name="IN_PROGRESS").first()
             if review_status is not None:
                review_task = Task(title="task in review", status=review_status)
                review_task.save()

             if done_status is None:
                  done_status = Status.objects.filter(name="IN_PROGRESS").first()
             if done_status is not None:
                done_task = Task(title="task is done", status=done_status)
                done_task.save()
