# Backend for Task application

## Development

Run the application:

```
    docker-compose -f docker-compose.yml build && docker-compose -f docker-compose.yml up 
```

Running the application with the `docker-compose.yml` file allows it to run with a real database and the
Keycloak server, and it uses the current version of the source code, since the source code directory is
mapped via a volume into the docker container running the backend. That means one can run just
`docker-compose -f docker-compose.yml up`  to use the current version of the application in development.

To include the frontend when running the application:

```
    docker-compose -f docker-compose.yml --profile full build && docker-compose -f docker-compose.yml --profile full up 
```

When running without the frontend, a local frontend can be started via:

```
  cd ../frontend
  npm install
  npm start
```

When the application is running, connect to it on the shell:

```
  docker exec -it task-api /bin/bash
```

The docker-compose setup creates three or four containers: task-api, task-postgres, task-keycloak,
and the optional task-frontend.

The services in those containers can be accessed from the server running the docker containers:

* React frontend: http://localhost:3000
* Django backend: http://localhost:8000
* Postgres DB: The DB is available on port $DB_PORT as defined in the file `.env` 
  (set to 2345, so it does not collide with a locally running PostgreSQL)
* Keycloak: http://localhost:8080; when /etc/hosts maps the hostnam `keycloak` to 127.0.0.1,
  it can also be accessed via http://keycloak:8080 

## Setup

To work directly on the database on the postgres server:

```
   docker exec -it task-postgres /bin/bash

  > psql -h localhost -p 2345 -U task-user task
  > createdb -h localhost -p 2345 -U task-user <db-name>
  > dropdb -h localhost -p 2345 -U task-user <db-name>
```

## Create Keycloak config

Keycloak config:
* Go to Keycloak admin (http://localhost:8080 in our docker-compose setup)
* Create realm (top left dropdown, click on "master", then "Create"), give it the name "task"
* Select realm in top-left dropdown
* Create user with password credentials
* Create client with client-id "webapp", enable client authentication, save
* Copy client secret from tab "Credentials"

### Create a user

**TODO** Check how this is supposed to work..

* First, create the user with password on the keycloak server
* Then, we need to login with this user via Django app
* This should create the user object in the DB
* For this user, some other stuff needs to be done (see below)

